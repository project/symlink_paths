<?php

/**
 * @file
 * SymlinkPaths streamwrapper.
 */

/**
 * Symbolic Link (symlink://) stream wrapper class.
 */
class SymlinkPathsStreamWrapper extends DrupalPublicStreamWrapper {

  /**
   * Returns the canonical absolute path of the URI, if possible.
   *
   * Overrides getLocalPath to allow symlinked locations
   * see: https://www.drupal.org/node/1008402#comment-9065341
   *
   * @param string $uri
   *   (optional) The stream wrapper URI to be converted to a canonical
   *   absolute path. This may point to a directory or another type of file.
   *
   * @return string|false
   *   If $uri is not set, returns the canonical absolute path of the URI
   *   previously set by the DrupalStreamWrapperInterface::setUri() function.
   *   If $uri is set and valid for this class, returns its canonical absolute
   *   path, as determined by the realpath() function. If $uri is set but not
   *   valid, returns FALSE.
   */
  protected function getLocalPath($uri = NULL) {
    if (!isset($uri)) {
      $uri = $this->uri;
    }
    // This is the target path relative to the files repository.
    $target = DIRECTORY_SEPARATOR . $this->getTarget($uri);
    // This is the files repository directory.
    $repository = realpath($this->getDirectoryPath());
    // This is the target directory.
    $target_dir = realpath(dirname($repository . $target)) . DIRECTORY_SEPARATOR;

    // This is the target name, without any directory components.
    // drupal_basename suggested by OnkelTem
    // <OnkelTem@239962.no-reply.drupal.org>.
    $target_name = drupal_basename($repository . $target);
    // A directory component can point outside its parent directory if the path
    // separator ('/' or '\') is followed either by '..' to reference the parent
    // directory, or '~' to reference the user home directory.
    $pattern = '@(/|\\\\)(\.\.|~)@';
    // This checks whether the target can possibly point outside its parent.
    $traversal = preg_match($pattern, $target);
    // This checks whether the target dir exists within the files repository.
    $subdirectory = strpos($target_dir, $repository . DIRECTORY_SEPARATOR) === 0;
    if ($traversal && !$subdirectory) {
      // If the target path contains directory-traversal parts such as
      // '/..' or '/~', and does not resolve to a subdirectory of the
      // repository, then return FALSE to avoid a possible exploit.
      return FALSE;
    }
    return $target_dir . $target_name;
  }

  /**
   * Implements abstract public function getDirectoryPath().
   *
   * @return string
   *   returns symlink path if configured or the default path
   */
  public function getDirectoryPath() {
    return variable_get('symlink_paths_path', conf_path() . '/files');
  }

  /**
   * Overrides DrupalPublicStreamWrapper::getExternalUrl().
   *
   * Return the HTML URI of a public file.
   */
  public function getExternalUrl() {
    $path = str_replace('\\', '/', $this->getTarget());
    return $GLOBALS['base_url'] . '/' . self::getDirectoryPath() . '/' . drupal_encode_path($path);
  }

}
