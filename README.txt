
INTRODUCTION
------------

Readme for symlink_paths

When uploading files to the local file store on the server drupal defaults to
disallowing symlinked locations outside of the document root.

This is a security feature which is designed to prevent file uploads being
crafted to give access to system files. When files are saved a 'streamwrapper'
is called which handles the saving of the file and also does the check for
symlinked 'out of docroot' paths

This module creates a new streamwrapper based on the core
DrupalPublicStreamWrapper and overrides the function getLocalPath($uri=null)

When implemented it is enough to choose "symlinked document store" for your
file field. Additional checks are performed to prevent security issues.

The file will then be saved into the symlinked folder without the usual errors

The code in this module is based on the patch found here
https://www.drupal.org/node/1008402#comment-9065341

Currently this code works in Drupal 7 only.


REQUIREMENTS
------------

 * Fields (core) module


HOW TO INSTALL
--------------

 * Copy folder to sites/all/modules
 * Browse to admin/modules in your browser
 * Enable Symlink Paths in the Stream Wrappers section
 * Create a symlink in your files folder
   e.g. sites/default/files/documentstore -> /documentstore
 * Make sure your symlinked folder is writeable by the webserver
 * Navigate to admin/config/media/file-system to configure the symlinked
   path.

HOW TO USE
----------

 * When the module is installed and enabled creating a new file field in your
   content type will give you the option to store files in either
   public: private: or "symlinked document store:"
   choose "symlinked document store"
 * Now when you upload a file to your server it will be copied to the
   appropriate folder.
 * Make sure your webserver will follow symlinks for example in apache you
   need the configuration setting "Options FollowSymLinks"


That should be it!


RECOMMENDED MODULES
-------------------
 * filefield_paths - allows amongst other things token replacements in the path


MAINTAINERS
-----------

Current maintainers:
 * DeveloperChris - https://www.drupal.org/project/user/2789879
